provider "kubernetes" {
  host  = civo_kubernetes_cluster.primary.api_endpoint
  client_certificate = base64decode(
    yamldecode(civo_kubernetes_cluster.primary.kubeconfig).users[0].user.client-certificate-data
  )
  client_key = base64decode(
    yamldecode(civo_kubernetes_cluster.primary.kubeconfig).users[0].user.client-key-data
  )
  cluster_ca_certificate = base64decode(
    yamldecode(civo_kubernetes_cluster.primary.kubeconfig).clusters[0].cluster.certificate-authority-data
  )
}
